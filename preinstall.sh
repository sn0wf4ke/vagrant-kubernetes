echo -e "vagrant\nvagrant" | sudo passwd vagrant
export DEBIAN_FRONTEND=noninteractive
cp /vagrant/.vault /home/vagrant/
sudo chmod 600 /home/vagrant/.vault     
sudo chown vagrant:vagrant /home/vagrant/.vault     
ansible --version || (  
sudo apt-get -y purge python2.7 python-minimal
# sudo echo 'Acquire::http::Proxy "http://192.168.5.251:3142/";' > /etc/apt/apt.conf.d/50proxy.conf
sudo apt-get update && sudo apt-get -q --option \"Dpkg::Options::=--force-confold\" --assume-yes upgrade -y -qq
sudo -E apt-get -q --option \"Dpkg::Options::=--force-confold\" --assume-yes install -y -qq python3-pip
sudo mkdir -p /vagrant/sources/pip3
cd /vagrant/sources/pip3
sudo pip3 download -r /vagrant/requirements.txt
sudo mkdir -p /etc/xdg/pip
sudo cp /vagrant/pip.conf /etc/xdg/pip/pip.conf
sudo pip3 install ansible )
