- name: install and configure docker
  hosts: all
  roles:
    - role: docker
      become: 'yes'
      docker__edition: "ce"
      docker__channel: ["stable"]
      docker__version: '20.10'
      docker__daemon_flags:
        - "-H unix://"
        - "--exec-opt native.cgroupdriver=systemd"
        - "--log-opt max-size=25m"
        - "--log-opt max-file=3"
      docker__daemon_environment: []
      docker__systemd_override: ""
      docker__users: ["{{ ansible_user }}"]
      docker__default_daemon_json: |
        "insecure-registries" : ["{{ docker_registry }}"]
  tasks:
    - name: ensure docker daemon is running
      become: 'yes'
      systemd:
        name: docker
        state: started

    - name: "ensure {{ k8_admin_node }} is resolvable on all nodes"
      become: 'yes'
      lineinfile:
        path: /etc/hosts
        line: "{{ hostvars[k8_admin_node].ansible_default_ipv4.address }} {{ k8_admin_node }}"

- name: deploy docker registry
  hosts: docker_registry
  vars:
    registry_path: /srv/docker-registry
  tasks:
    - name: Create local volume and configure SELinux
      become: 'yes'
      file:
        path: "{{ registry_path }}"
        setype: svirt_sandbox_file_t
        state: directory
      run_once: 'yes'

    - name: ensure container images download folder
      file:
        path: '{{ sources_path }}/container_images'
        state: directory

    - name: get docker registry image
      block:
        - name: read docker registry image
          become: 'yes'
          docker_image:
            name: 'registry:2'
            source: 'load'
            force_source: 'yes'
            load_path: '{{ sources_path }}/container_images/registry~2.tar.gz'
          delegate_to: "{{ k8_admin_node }}"
          run_once: 'yes'
          when: deploy_registry
      rescue:
        - name: pull and save docker registry image
          become: 'yes'
          docker_image:
            name: 'registry:2'
            source: 'pull'
            force_source: 'yes'
            archive_path: '{{ sources_path }}/container_images/registry~2.tar.gz'
          run_once: 'yes'

    - name: Deploy Docker Registry
      become: 'yes'
      docker_container:
        container_default_behavior: compatibility
        image: "registry:2"
        name: local-registry
        ports: "5000:5000"
        volumes: "{{ registry_path }}:/srv/registry"
        env:
          STANDALONE: "true"
          MIRROR_SOURCE: "https://registry-1.docker.io"
          MIRROR_SOURCE_INDEX: "https://index.docker.io"
          STORAGE_PATH: "/srv/registry"
        restart_policy: always
      run_once: 'yes'
      when: deploy_registry

    - name: read, tag and push all images to local-registry
      register: result
      become: 'yes'
      docker_image:
        name: '{{ item }}'
        repository: '{{ docker_registry }}/{{ item.split("/") | last }}'
        push: 'yes'
        source: 'load'
        force_source: 'yes'
        force_tag: 'yes'
        load_path: '{{ sources_path }}/container_images/{{ item.split("/") | 
          last | 
          replace(":","~") }}.tar.gz'
      delegate_to: "{{ k8_admin_node }}"
      with_items: '{{ container_images }}'
      run_once: 'yes'
      ignore_errors: 'yes'

    - name: pull, save, tag and push missing images to local-registry
      register: result
      become: 'yes'
      docker_image:
        name: '{{ item.item }}'
        repository: '{{ docker_registry }}/{{ item.item.split("/") | last }}'
        push: 'yes'
        source: 'pull'
        force_source: 'yes'
        force_tag: 'yes'
        archive_path: '{{ sources_path }}/container_images/{{ item.item.split("/") | 
          last | 
          replace(":","~") }}.tar.gz'
      with_items: '{{ result.results | selectattr("failed") }}'
      loop_control:
        label: '{{ item.item }}'
      retries: 5
      until: result is succeeded
      run_once: 'yes'
