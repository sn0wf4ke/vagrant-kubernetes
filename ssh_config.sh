#!/bin/bash
set -x
mkdir -p /vagrant/ssh-config 
VIRT=$( sudo dmidecode | sed -n 's/Product\sName:\s\([^\s]\)/\1/p' | head -n1 )
INTERFACE="eth0"
if [ $VIRT == "VirtualBox" ]; then
INTERFACE="eth1"
# remove virtualbox default route
sudo ip route del default via 10.0.2.2
fi
FILE=/vagrant/ssh-config/$1_id_rsa 
SSH_CONFIG=/vagrant/ssh-config/config 
IP_ADDRESS=$( ip -br -4 address | grep $INTERFACE | grep -Po '(\d+\.){3}\d+' )
if [ $1 == "worker1" ]; then
  rm -f $SSH_CONFIG
fi
if !( test -f $FILE ) ; then 
  ssh-keygen -t rsa -f "$FILE" -P '' 
fi
if [ $( cat /home/vagrant/.ssh/authorized_keys | wc -l ) == "1" ]; then
  cat /vagrant/ssh-config/$1_id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
fi
echo "Host $1" >> "$SSH_CONFIG" 
echo "Hostname $IP_ADDRESS " >> "$SSH_CONFIG" 
echo "User vagrant" >> "$SSH_CONFIG" 
echo "Port 22" >> "$SSH_CONFIG" 
echo "UserKnownHostsFile /dev/null" >> "$SSH_CONFIG" 
echo "StrictHostKeyChecking no" >> "$SSH_CONFIG" 
echo "PasswordAuthentication no" >> "$SSH_CONFIG" 
echo "IdentityFile ~/.ssh/$1_id_rsa" >> "$SSH_CONFIG" 
echo "IdentitiesOnly yes" >> "$SSH_CONFIG" 
echo "LogLevel FATAL" >> "$SSH_CONFIG"
