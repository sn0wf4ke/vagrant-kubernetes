# Kubernetes Cluster LAB and Offline Collector

The aim for this repository is to deploy a kubernetes cluster on an internet connected system and transfer it to deploy it in offline environment again.

## Usage

### internet connected system

* the `Vagrant` setup is `hyperv` or `virtualbox` based so you need a working vagrant environment with one of these providers and with internet connection and `24GB` free RAM (can be adjusted in `Vagrantfile`)
* `ubuntu 20.04` is used as vagrant box

* first you have to setup the following environment variables to your needs:

```pwsh
$env:BRIDGE="<your bridge to public network>"
```

* for hyperv additional set your credentials to mount smb synced folder

```pwsh
$env:SMB_PASSWORD="<your password>"
$env:SMB_USER="<your user>"
```

* then you have to adjust the ansible inventory `./inventory`: 
  * `./inventory/group_vars/all/kubernetes.yml` -> the variable `k8_kubeapi_vip` to a free address of your public network
  * `./pip.conf` -> pip configuration matching to your environment
  * `preinstall.sh` -> uncomment and change the apt proxy line if needed

* then it's time to fire up the Vagrantfile with:

```pwsh
vagrant up --color
```

this will:

* create virtual machines
* download all needed pip packages from `requirements.txt`
* install ansible on last master node
* download docker `deb` binaries and install them
* download and deploy a docker registry on last master node when `deploy_registry` is `true`
* download needed container images and push them to docker-registry 
* prepare hosts for kubernetes deployment
* download, save and install kubernetes `deb` binaries
* deploy and configure `haproxy` and `keepalived` on all control-plane nodes
* deploy kubernetes 3 node control-plane
* make last master a admin-node to use `kubectl`
* install `canal` network interface
* deploy 3 workers

### offline environment

after this copy the project folder to offline environment to deploy it again with

```pwsh
vagrant up --color
```

or adjust the `ansible inventory` to your needs to deploy the cluster on baremetal or prepared virtual environment

### usage

* use `master3` to manage the cluster:

```pwsh
vagrant ssh master3
kubectl -n kube-system get pods
```
